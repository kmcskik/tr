package com.example.km.travelrepublic.network;

import com.example.km.travelrepublic.models.hotels.HotelDealsRequest;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface HotelDealsRequestInterface {

    @POST("api/hotels/deals/search/")
    Call<ResponseBody> getHotelDeals(@Body HotelDealsRequest request, @Query("fields") String fields);

    @POST
    Call<ResponseBody> getHotelDeals(@Url String url, @Body HotelDealsRequest request);
}
