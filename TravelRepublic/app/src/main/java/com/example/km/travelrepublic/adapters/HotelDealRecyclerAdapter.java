package com.example.km.travelrepublic.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.km.travelrepublic.R;
import com.example.km.travelrepublic.models.hotels.HotelsByChildDestination;
import com.example.km.travelrepublic.viewholders.HotelDealRecyclerViewHolder;

import java.util.TreeMap;

/**
 * HotelDealRecyclerAdapter.
 */
public class HotelDealRecyclerAdapter extends RecyclerView.Adapter<HotelDealRecyclerViewHolder> {

    protected HotelsByChildDestination hotelDeal;
    private TreeMap<Integer, HotelsByChildDestination> hotelDeals;
    protected LayoutInflater inflater;

    public HotelDealRecyclerAdapter(Context context, TreeMap<Integer, HotelsByChildDestination> hotelDeals) {
        this.hotelDeals = hotelDeals;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public HotelDealRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.card_layout, parent, false);
        HotelDealRecyclerViewHolder holder = new HotelDealRecyclerViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(HotelDealRecyclerViewHolder holder, int position) {
        HotelsByChildDestination hotelDeal = this.hotelDeals.get(position);
        holder.setData(hotelDeal, position);
    }

    @Override
    public int getItemCount() {
        return hotelDeals.size();
    }
}
