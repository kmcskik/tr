package com.example.km.travelrepublic.fragments;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.km.travelrepublic.R;
import com.example.km.travelrepublic.adapters.AndroidVersionDataAdapter;
import com.example.km.travelrepublic.models.versions.AndroidVersion;
import com.example.km.travelrepublic.network.AndroidVersionRequestInterface;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class VersionsFragment extends Fragment {

    public static final String BASE_URL = "https://api.learn2crack.com/";
    private View progressOverlay;
    private RecyclerView recyclerView;
    private CompositeDisposable disposables;
    private AndroidVersionDataAdapter adapter;
    private ArrayList<AndroidVersion> androidArrayList;

    public VersionsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.fragment_versions, container, false);
        setupScreenLayout(view);
        disposables = new CompositeDisposable();
        initializeRecyclerView();
        loadVersions2();
        //loadVersions();
        return view;
    }

    private void initializeRecyclerView() {
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
    }

    /**
     * Get raw JSON string from retrofit call.
     */
    private void loadVersions2() {
        AndroidVersionRequestInterface requestInterface = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                //.addConverterFactory(GsonConverterFactory.create())
                .build().create(AndroidVersionRequestInterface.class);

        Call<ResponseBody> responseCall = requestInterface.register2();
        ;
        responseCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    String body = response.body().string();
                    int code = response.code();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                loadVersions();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                String error = t.getMessage();
                loadVersions();
            }
        });
    }

    /**
     * Load JSON into POJO class.
     */
    private void loadVersions() {
        showProgressIndicator();
        AndroidVersionRequestInterface requestInterface = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(AndroidVersionRequestInterface.class);

        Observable<List<AndroidVersion>> resultObservable = requestInterface.register();
        disposables.add(resultObservable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableObserver<List<AndroidVersion>>() {
                    @Override
                    public void onComplete() {
                        hideProgressIndicator();
                        Snackbar.make(recyclerView, "API call complete", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        handleError(throwable);
                    }

                    @Override
                    public void onNext(List<AndroidVersion> androidList) {
                        handleResponse(androidList);
                    }
                }));
    }

    private void handleResponse(List<AndroidVersion> androidList) {
        androidArrayList = new ArrayList<>(androidList);
        adapter = new AndroidVersionDataAdapter(androidArrayList);
        recyclerView.setAdapter(adapter);
    }

    private void handleError(Throwable error) {
        Snackbar.make(recyclerView, "Error " + error.getLocalizedMessage(), Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
        Toast.makeText(getActivity(), "Error " + error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().finish();
                return true;
            case R.id.action_home:
                return true;
            case R.id.action_settings:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        disposables.clear();
    }

    public void showProgressIndicator() {
        progressOverlay.setVisibility(View.VISIBLE);
    }

    public void hideProgressIndicator() {
        progressOverlay.setVisibility(View.GONE);
    }

    private void setupScreenLayout(View view) {
        progressOverlay = view.findViewById(R.id.progressOverlay);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
    }
}
