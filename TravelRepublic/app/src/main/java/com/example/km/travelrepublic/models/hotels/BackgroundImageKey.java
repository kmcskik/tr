package com.example.km.travelrepublic.models.hotels;

import com.example.km.travelrepublic.constants.Constants;

public class BackgroundImageKey {

    private String type;
    private String id;

    public BackgroundImageKey(String key) {
        String[] parts = key.split(Constants.PIPE);
        this.type = parts[0];
        this.id = parts[1];
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
