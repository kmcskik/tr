package com.example.km.travelrepublic.models.hotels;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.TreeMap;

public class HotelsByChildDestinationContainer {

    public HotelsByChildDestinationContainer() {
    }

    /**
     * Load hotel deals from file.
     */
    public TreeMap<Integer, HotelsByChildDestination> loadHotelDealsFromFile() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        String json = loadJson();
        JsonElement root = null;
        try {
            root = new JsonParser().parse(json);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        TreeMap<Integer, HotelsByChildDestination> hotelDealsSortedByPosition = new TreeMap<Integer, HotelsByChildDestination>();
        JsonObject object = root.getAsJsonObject().get(HotelsByChildDestination.class.getSimpleName()).getAsJsonObject();
        for (Map.Entry<String, JsonElement> entry : object.entrySet()) {
            HotelsByChildDestination hotel = gson.fromJson(entry.getValue(), HotelsByChildDestination.class);
            hotel.setBackgroundImageKey(entry.getKey());
            hotelDealsSortedByPosition.put(hotel.position, new HotelsByChildDestination(hotel));
        }
        return hotelDealsSortedByPosition;
    }

    /**
     * Load hotels.json from a file.
     */
    public String loadJson() {
        String json = null;
        try {
            InputStream is = this.getClass().getClassLoader().getResourceAsStream("hotels.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
