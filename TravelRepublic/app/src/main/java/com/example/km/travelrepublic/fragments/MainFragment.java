package com.example.km.travelrepublic.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.example.km.travelrepublic.R;
import com.example.km.travelrepublic.adapters.HotelDealRecyclerAdapter;
import com.example.km.travelrepublic.constants.Constants;
import com.example.km.travelrepublic.models.hotels.HotelDealsRequest;
import com.example.km.travelrepublic.models.hotels.HotelsByChildDestination;
import com.example.km.travelrepublic.network.AndroidVersionRequestInterface;
import com.example.km.travelrepublic.network.HotelDealsRequestInterface;
import com.example.km.travelrepublic.utilities.SnackbarHelper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;
import java.util.TreeMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainFragment extends Fragment {

    //public static final String BASE_URL_1 = "https://www.travelrepublic.co.uk/api/hotels/deals/search/";
    public static final String BASE_URL_1 = "https://www.travelrepublic.co.uk/";
    public static final String BASE_URL_2 = "https://api.learn2crack.com/";
    private View progressOverlay;
    private RecyclerView recyclerView;
    private TreeMap<Integer, HotelsByChildDestination> hotelDeals;
    private HotelDealRecyclerAdapter adapter;

    public MainFragment() {
    }

    public HotelDealRecyclerAdapter getAdapter() {
        return adapter;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        setupScreenLayout(view);
        initializeRecyclerView();
        fetchHotelDealsFromApi2();
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().finish();
                return true;
            case R.id.action_home:
                return true;
            case R.id.action_settings:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public void showProgressIndicator() {
        progressOverlay.setVisibility(View.VISIBLE);
    }

    public void hideProgressIndicator() {
        progressOverlay.setVisibility(View.GONE);
    }

    private void setupScreenLayout(View view) {
        progressOverlay = view.findViewById(R.id.progressOverlay);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
    }

    private void initializeRecyclerView() {
        LinearLayoutManager linearLayoutManagerVertical = new LinearLayoutManager(getActivity());
        linearLayoutManagerVertical.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManagerVertical);
    }

    private void setRecyclerViewAdapter(String json) {
        hotelDeals = loadHotelDeals(json);
        adapter = new HotelDealRecyclerAdapter(getActivity(), hotelDeals);
        recyclerView.setAdapter(adapter);
    }

    /**
     * Get raw JSON string from retrofit call.
     */
    private void fetchHotelDealsFromApi1() {
        showProgressIndicator();
        HotelDealsRequestInterface requestInterface = new Retrofit.Builder()
                .baseUrl(BASE_URL_1)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(HotelDealsRequestInterface.class);
        HotelDealsRequest searchRequest = new HotelDealsRequest();
        searchRequest.CheckInDate = "2017-10-14T00:00:00.000Z";
        searchRequest.Flexibility = 3;
        searchRequest.Duration = 7;
        searchRequest.Adults = 1;
        searchRequest.DomainId = 1;
        searchRequest.CultureCode = "en-gb";
        searchRequest.CurrencyCode = "GBP";
        String[] airports = new String[]{"LHR", "LCY", "LGW", "LTN", "STN", "SEN"};
        searchRequest.OriginAirports = airports;
        searchRequest.FieldFlags = 8143571;
        searchRequest.IncludeAggregates = true;
//        Call<ResponseBody> responseCall = requestInterface.getHotelDeals(searchRequest, "Aggregates.HotelsByChildDestination");
        String url = getUrl("Aggregates.HotelsByChildDestination");
        Call<ResponseBody> responseCall = requestInterface.getHotelDeals(url, searchRequest);
        responseCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                String body = Constants.EMPTY_STRING;
                String error = Constants.EMPTY_STRING;
                try {
                    error = response.errorBody().string();
                    body = response.body().string();
                } catch (IOException e) {
                    SnackbarHelper.showSnackbar(getActivity(), recyclerView, error);
                    e.printStackTrace();
                }
                hideProgressIndicator();
                setRecyclerViewAdapter(body);
                SnackbarHelper.showSnackbar(getActivity(), recyclerView, "API call complete");
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideProgressIndicator();
                SnackbarHelper.showSnackbar(getActivity(), recyclerView, "Error " + t.getLocalizedMessage());
            }
        });
    }

    private String getUrl(String searchTerm) {
        String url = null;
        String encodedSearchTerm = searchTerm;
        try {
            encodedSearchTerm = URLEncoder.encode(searchTerm, "UTF-8");
        } catch (UnsupportedEncodingException e1) {
            return url;
        }
        String website = "https://www.travelrepublic.co.uk/api/hotels/deals/search";
        url = String.format("%s?fields=%s", website, searchTerm);
        return url;
    }

    /**
     * Get raw JSON string from retrofit call.
     */
    private void fetchHotelDealsFromApi2() {
        showProgressIndicator();
        AndroidVersionRequestInterface requestInterface = new Retrofit.Builder()
                .baseUrl(BASE_URL_2)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build().create(AndroidVersionRequestInterface.class);

        Call<ResponseBody> responseCall = requestInterface.register2();
        ;
        responseCall.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                String body = "";
                try {
                    body = response.body().string();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                hideProgressIndicator();
                //setRecyclerViewAdapter(body);
                setRecyclerViewAdapter(fetchJsonFromFile());
                SnackbarHelper.showSnackbar(getActivity(), recyclerView, "API call complete");
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                hideProgressIndicator();
                SnackbarHelper.showSnackbar(getActivity(), recyclerView, "Error " + t.getLocalizedMessage());
            }
        });
    }

    /**
     * Load hotel deals from JSON string.
     */
    public TreeMap<Integer, HotelsByChildDestination> loadHotelDeals(String json) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        JsonElement root = null;
        try {
            root = new JsonParser().parse(json);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        TreeMap<Integer, HotelsByChildDestination> hotelDealsSortedByPosition = new TreeMap<Integer, HotelsByChildDestination>();
        JsonObject object = root.getAsJsonObject().get(HotelsByChildDestination.class.getSimpleName()).getAsJsonObject();
        for (Map.Entry<String, JsonElement> entry : object.entrySet()) {
            HotelsByChildDestination hotel = gson.fromJson(entry.getValue(), HotelsByChildDestination.class);
            hotel.setBackgroundImageKey(entry.getKey());
            hotelDealsSortedByPosition.put(hotel.position, new HotelsByChildDestination(hotel));
        }
        return hotelDealsSortedByPosition;
    }

    /**
     * Get hotels.json from a file.
     */
    public String fetchJsonFromFile() {
        String json = null;
        try {
            InputStream is = getActivity().getAssets().open(getString(R.string.hotels_json_file));
            //Used the line below when running in a unit test.
            //InputStream is = this.getClass().getClassLoader().getResourceAsStream("hotels.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
