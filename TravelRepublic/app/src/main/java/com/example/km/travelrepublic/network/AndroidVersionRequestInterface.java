package com.example.km.travelrepublic.network;

import com.example.km.travelrepublic.models.versions.AndroidVersion;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;

public interface AndroidVersionRequestInterface {

    @GET("android/jsonarray/")
    Observable<List<AndroidVersion>> register();

    @GET("android/jsonarray/")
    Call<ResponseBody> register2();
}
