package com.example.km.travelrepublic.models.hotels;

public class HotelDealsRequest {
    public String CheckInDate;
    public int Flexibility;
    public int Duration;
    public int Adults;
    public int DomainId;
    public String CultureCode;
    public String CurrencyCode;
    public String[] OriginAirports;
    public int FieldFlags;
    public boolean IncludeAggregates;
}
