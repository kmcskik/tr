package com.example.km.travelrepublic.models.hotels;

import com.google.gson.annotations.SerializedName;

import java.text.NumberFormat;

public class HotelsByChildDestination {

    @SerializedName("Title")
    public String title;
    @SerializedName("Count")
    public int count;
    @SerializedName("MinPrice")
    public double minPrice;
    @SerializedName("Position")
    public int position;

    private BackgroundImageKey backgroundImageKey;

    public HotelsByChildDestination(HotelsByChildDestination hotelDeal) {
        this.title = hotelDeal.getTitle();
        this.count = hotelDeal.getCount();
        this.minPrice = hotelDeal.getMinPrice();
        this.position = hotelDeal.getPosition();
        this.backgroundImageKey = hotelDeal.getBackgroundImageKey();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String input) {
        this.title = input;
    }

    public int getCount() {
        return count;
    }

    public String getCountDisplay()
    {
        return String.format("(%s)", count);
    }

    public void setCount(int input) {
        this.count = input;
    }

    public double getMinPrice() {
        return minPrice;
    }

    public String getMinPriceDisplay() {
        NumberFormat format = NumberFormat.getCurrencyInstance();
        return format.format(minPrice);
    }

    public void setMinPrice(double input) {
        this.minPrice = input;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int input) {
        this.position = input;
    }

    public BackgroundImageKey getBackgroundImageKey() {
        return backgroundImageKey;
    }

    public void setBackgroundImageKey(String key) {
        this.backgroundImageKey = new BackgroundImageKey(key);
    }
}
