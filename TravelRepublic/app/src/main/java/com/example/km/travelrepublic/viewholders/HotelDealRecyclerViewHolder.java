package com.example.km.travelrepublic.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.km.travelrepublic.R;
import com.example.km.travelrepublic.models.hotels.HotelsByChildDestination;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

/**
 * HotelDealRecyclerViewHolder.
 */
public class HotelDealRecyclerViewHolder extends RecyclerView.ViewHolder {

    private static final String URL_STEM = "https://d2f0rb8pddf3ug.cloudfront.net/api2/destination/images/getfromobject?";
    protected int position;
    protected HotelsByChildDestination hotelDeal;
    protected ImageView backgroundImageView;
    protected TextView titleTextView;
    protected TextView countTextView;
    protected TextView minPriceTextView;

    public HotelDealRecyclerViewHolder(View view) {
        super(view);
        backgroundImageView = (ImageView) view.findViewById(R.id.backgroundImage);
        titleTextView = (TextView) view.findViewById(R.id.title);
        countTextView = (TextView) view.findViewById(R.id.count);
        minPriceTextView = (TextView) view.findViewById(R.id.minPrice);
    }

    public void setData(final HotelsByChildDestination hotelDeal, int position) {
        this.position = position;
        this.hotelDeal = hotelDeal;
        //TODO: set backgroundImageView by loading appropriate url etc.
        titleTextView.setText(hotelDeal.getTitle());
        countTextView.setText(hotelDeal.getCountDisplay());
        minPriceTextView.setText(hotelDeal.getMinPriceDisplay());
        String url = buildUrl(hotelDeal.getBackgroundImageKey().getId(), hotelDeal.getBackgroundImageKey().getType());
        loadImageByUrl(backgroundImageView, url);
        backgroundImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
    }

    private String buildUrl(String id, String type) {
        return URL_STEM + "id=" + id + "&type=" + type + "&useDialsImages=true&width=300&height=300";
    }

    private void loadImageByUrl(ImageView view, String url) {
        RequestCreator creator = Picasso.with(view.getContext()).load(url);
        creator.into(view);
    }
}
