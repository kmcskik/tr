package com.example.km.travelrepublic.interfaces;

public interface NavigationDrawerMenuItemListener {
    public void onNavigationDrawerMenuItemSelected(int selectedPosition);
}
