package com.example.km.travelrepublic.utilities;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.example.km.travelrepublic.R;

/**
 * Snackbar helper.
 */
public final class SnackbarHelper {

    public static void showSnackbar(Context context, View view, String message) {
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG)
                .setAction("Action", null);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(ContextCompat.getColor(context, R.color.DarkOrange));
        snackbar.show();
    }
}
