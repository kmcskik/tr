package com.example.km.travelrepublic.models.navigation;

/**
 * NavigationDrawerItem Class.
 */
public class NavigationDrawerItem {

    public int icon;
    public String name;

    // Constructor.
    public NavigationDrawerItem(int icon, String name) {
        this.icon = icon;
        this.name = name;
    }
}
