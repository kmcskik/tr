package com.example.km.travelrepublic;

import com.example.km.travelrepublic.models.hotels.HotelsByChildDestinationContainer;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * FAQ container unit test..
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class HotelsByChildDestinationContainerUnitTest {

    @Before
    public void setup() throws Exception {
    }

    @Test
    public void readAndParseJsonTest() throws Exception {
        HotelsByChildDestinationContainer container = new HotelsByChildDestinationContainer();
        assertEquals(4, 2 + 2);
    }
}
